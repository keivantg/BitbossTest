<?php

namespace App\Http\Controllers;

use App\Models\Application;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function homepage() {
        return view('home');
    }

    public function apply() {
        $application = Application::where("user_id", \Auth::user()->id)->first();
        $link = route('myApply');
        return view('apply', compact('application', 'link'));
    }

    public function myApply(){

        $application = Application::where("user_id", \Auth::user()->id)->first();
        $link = route('apply');

        return view('myApply', compact('application', 'link'));
    }

    public function postApply(Request $request) {

        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'phone' => 'required',
        ],
            [
                "first_name.required" => "Nome campo obbligatorio",
                "last_name.required" => "Cognome campo obbligatorio",
                "email.required" => "Email campo obbligatorio",
                "phone.required" => "Telefono campo obbligatorio",
            ]
        );


        $link = route('myApply');

        $link_html = "<a href='$link'><br>Clicca qui per vedere lo stato della tua candidatura.</a>";

        $application = Application::where("user_id", \Auth::user()->id)->first();
        if($application){
            $date = Carbon::createFromFormat("Y-m-d H:i:s", $application->created_at)->format("d/m/Y H:i");

            $message = "Ti sei già candidato per questa offerta lavorativa in data <strong>$date</strong>. $link_html";
            return redirect()->back()->withInput()->withErrors(["$message"]);
        }

        $application = Application::create($request->except('_token'));

        try {
            \Log::channel('slack')
                ->alert('Nuova candidatura', [
                    'url' => url()->current(),
                    "message" => "Nuova candidatura"
                ]);

            $users_admin = User::where("is_admin", 1)->get();
            if($users_admin){
                foreach ($users_admin as $admin){
                    $destinatario = $admin->email;

                    \Mail::send("emails.candidatura", ['data' => $application], function ($m) use ($destinatario) {
                        $m->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
                        $m->to($destinatario)->subject("Nuova candidatura");
                    });
                }
            }
        } catch (\Throwable $e) {

        }



        return redirect()->back()
            ->with('message', "Candidatura avvenuta con successo! $link_html");

    }
}
