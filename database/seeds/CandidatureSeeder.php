<?php

use Illuminate\Database\Seeder;

class CandidatureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $email = "k1@test.it";
        $name = "Keivan Tahami";

        $user = \App\Models\User::create([
           "name" => $name,
           "email" => $email,
           "password" => bcrypt("12345678")
        ]);

        \App\Models\Application::create([
           "first_name"  => "Keivan",
           "last_name" => "Tahami",
           "email" => "k1@test.it",
           "phone" => "1111",
           "user_id" => $user->id
        ]);
    }
}
