@extends('layouts.app')

@section('content')
    <section class="px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <img src="//bitboss.it/images/BitBoss/logo-black-200.png"/>
        <div class="container">
            <h1>Stato candidatura</h1>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="container">
                @if(!$application)
                    <div class="alert alert-warning text-center shadow-sm mb-4 alert-fixed-bottom-xs">
                        <h5>Non ti sei ancora candidato? Cosa aspetti!</h5>
                        <a href='{{ $link }}'><br>Clicca qui per candidarti.</a>
                    </div>
                @else
                    <p><strong>Data invio:</strong> {{ \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $application->created_at)->format("d/m/Y H:i") }}</p>
                    <p><strong>Stato:</strong> {{ $application->status }}</p>
                @endif
            </div>
        </div>
    </section>

@stop
