@extends('layouts.app')

@section('content')
    <section class="px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <img src="//bitboss.it/images/BitBoss/logo-black-200.png"/>
        <div class="container">
            <h1>Candidati</h1>
            <p class="lead">Presenta la tua candidatura</p>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="container">
                @if(!$application)
                    {!! Form::open(['route' => ['apply.store'], 'method' => 'post'])!!}

                    @if ($errors->any())
                        <div class="alert alert-danger text-center alert-fixed-bottom-xs">
                            @foreach ($errors->all() as $error)
                                <div><strong>{!! $error !!}</strong></div>
                            @endforeach
                        </div>
                    @endif

                    @if(session()->has('message'))
                        <div class="alert alert-success text-center shadow-sm mb-4 alert-fixed-bottom-xs">
                            <h5>{!! session()->get('message') !!}</h5>
                        </div>
                    @endif

                    <input type="hidden" name="user_id" value="{{ \Auth::user()->id }}">

                    <div class="form-group">
                        {!! Form::label('first_name', 'Nome') !!}
                        {!! Form::text('first_name', null, ['class' => 'form-control', 'placeholder'=>'Nome', 'required' => true]) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('last_name', 'Cognome') !!}
                        {!! Form::text('last_name', null, ['class' => 'form-control', 'placeholder'=>'Cognome', 'required' => true]) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('email', 'Email') !!}
                        {!! Form::text('email', \Auth::user()->email, ['class' => 'form-control', 'placeholder'=>'Email', 'required' => true, 'readonly' => true]) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('phone', 'Telefono') !!}
                        {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder'=>'Telefono', 'required' => true]) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('notes', 'Note') !!}
                        {!! Form::textarea('notes', null, ['rows'=>3, 'class' => 'form-control', 'placeholder'=>'Note']) !!}
                    </div>

                    <div>

                        <button type="submit" class="btn btn-lg btn-block btn-outline-primary"><i class="fa fa-paper-plane"
                                                                                                  aria-hidden="true"></i>
                            {{__("Invia candidatura")}}
                        </button>
                    </div>

                    {!! Form::close() !!}

                @else
                    <div class="alert alert-warning text-center shadow-sm mb-4 alert-fixed-bottom-xs">
                        <?php
                        $date = \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $application->created_at)->format("d/m/Y H:i");
                        ?>
                        <h5>Ti sei già candidato per questa offerta lavorativa in data <strong>{{ $date }}</strong></h5>
                        <a href='{{ $link }}'><br>Clicca qui per vedere lo stato della tua candidatura.</a>
                    </div>
                @endif
            </div>
        </div>
    </section>

@stop
